ThisBuild / version := "0.1.0-SNAPSHOT"

ThisBuild / scalaVersion := "2.13.8"

lazy val root = (project in file("."))
  .settings(
    name := "ScalaBrokerTester"
  )

libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.12" % Test
libraryDependencies += "org.apache.kafka" % "kafka-clients" % "3.2.0" % Test
libraryDependencies += "org.slf4j" % "slf4j-nop" % "1.7.36" % Test


