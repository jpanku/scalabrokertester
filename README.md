# Scala Broker Tester

Java tests to check connectivity to a valid kafka broker.
Includes Producer functionality to create records which can be read by Consumer tests.

## Requirements
* Java 11
* Sbt (Tested with sbt version 1.6.2)
* A Kafka Server running a valid Broker with constant message traffic. (Tested with Kafka
  version 2.13-3.2.0)

## Usage Guide

Download the project

`git clone git@gitlab.com:jpanku/scalabrokertester.git`

From the same directory where you downloaded change into the project directory:

`cd scalabrokertester`

Clean and compile

`sbt clean compile`

Open `config/java.config` file and edit according to the setup of the Kafka server you will use.
Recommended is you set `polling.time` to at least 60 seconds to check activity. Topic is needed to
be changed according to topic configured in the broker.

### Running tests

Run tests

`sbt test`

You should se something similar to this in the screen if the broker don't have traffic, and you didn't
change default values. If the broker have records produced in the configured topic you should also
see those records consumed.

```
[info] welcome to sbt 1.6.2 (Oracle Corporation Java 11)
[info] loading global plugins from C:\Users\Juan\.sbt\1.0\plugins
[info] loading settings for project scalabrokertester-build from plugins.sbt ...
[info] loading project definition from F:\gitlab\scalabrokertester\project
[info] loading settings for project root from build.sbt ...
[info] set current project to ScalaBrokerTester (in build file:/F:/gitlab/scalabrokertester/)
[info] compiling 1 Scala source to F:\gitlab\scalabrokertester\target\scala-2.13\test-classes ...
Loading properties...
Validating broker...
Validating topic...
Starting main broker consuming test...
Polling during 10 seconds ...
>>>> No records found during polling.
[info] Main:
[info] - There is a valid broker running
[info] - Is the configured topic valid
[info] - Can subscribe and poll the configured topic
[info] Run completed in 11 seconds, 829 milliseconds.
[info] Total number of tests run: 3
[info] Suites: completed 1, aborted 0
[info] Tests: succeeded 3, failed 0, canceled 0, ignored 0, pending 0
[info] All tests passed.
[success] Total time: 20 s, completed May 29, 2022, 11:24:04 PM
```

Explanation (Steps/Tests):
* Setup, which load configuration values from configuration file.
* Validate if there is a valid broker.
* Validate if configured topic is created in broker.
* Polling and consuming records from the topic and broker configured.