import org.apache.kafka.clients.admin.Admin
import org.apache.kafka.clients.consumer.{Consumer, ConsumerConfig, KafkaConsumer}
import org.apache.kafka.common.PartitionInfo
import org.apache.kafka.common.errors.WakeupException
import org.scalatest.{BeforeAndAfterAll, CancelAfterFailure}
import org.scalatest.concurrent.TimeLimits.failAfter
import org.scalatest.funsuite.AnyFunSuite

import java.io.FileReader
import java.time.Duration
import java.util
import java.util.{Collections, Properties}
import java.util.concurrent.atomic.AtomicBoolean
import scala.language.postfixOps
import scala.util.{Failure, Success, Try}

class Main extends AnyFunSuite with BeforeAndAfterAll with CancelAfterFailure {

  /** Properties handler */
  val (initErrorMsg, properties) = buildProperties("config/java.config")

  /** Configured topic */
  private val topic = properties.getOrDefault("topic", "").toString

  /** Generates the Properties objects from the configuration file and additional properties needed.
    * It is used a Try and in case any failure a new Properties object will be returned.
    * @param configFile
    *   Path + name of the configuration file to be used.
    * @return
    *   Properties object
    */
  def buildProperties(configFile: String): (String, Properties) = {
    println("Loading properties...")

    Try {
      val _properties = new Properties()
      _properties.load(new FileReader(configFile))
      _properties.put(
        "bootstrap.servers",
        s"${_properties.getOrDefault("bootstrap.servers.host", "localhost")}" +
          s":${_properties.getOrDefault("bootstrap.servers.port", "9092")}"
      )
      _properties.put(
        ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
        "org.apache.kafka.common.serialization.StringDeserializer"
      )
      _properties.put(
        ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
        "org.apache.kafka.common.serialization.StringDeserializer"
      )
      _properties.put(
        ConsumerConfig.GROUP_ID_CONFIG,
        _properties.getOrDefault("group.id", "test-consumer-group")
      )
      _properties
    } match {
      case Failure(e)     => (e.getMessage, new Properties)
      case Success(value) => ("", value)
    }
  }

  /** Starts a thread for "pollingTime" seconds and when finished close and wake the consumer.
    * @param consumer
    *   Kafka broker consumer
    * @param closed
    *   Thread safe flag intended to tell consumer to close
    * @param pollingTime
    *   Time in seconds the consumer should be polling
    */
  def startConsumerStopper(
      consumer: Consumer[String, String],
      closed: AtomicBoolean,
      pollingTime: Int
  ): Unit = {
    class StopperThread extends Thread {
      override def run(): Unit = {
        Thread.sleep(pollingTime * 1000)
        closed.set(true)
        consumer.wakeup()
      }
    }
    new StopperThread().start()
  }

  /** Make the consumer to start polling during the polling time configured and shows some
    * information about consumed records.
    * @param consumer
    *   Kafka broker consumer
    * @param closed
    *   Thread safe flag which tells method if consumer is closed.
    * @param topic
    *   Configured topic
    * @return
    *   Total of records consumed
    */
  def startPolling(
      consumer: Consumer[String, String],
      closed: AtomicBoolean,
      topic: String
  ): Long = {
    var totalRecords = 0
    try
      while ({ !closed.get }) {
        val records = consumer.poll(Duration.ofMillis(100)).records(topic)
        var currentCount = 0L
        records.forEach { r =>
          totalRecords += 1
          println(
            s"Consumed record with key ${r.key()} and value " +
              s"${r.value()}, and updated total count to ${currentCount += 1; currentCount }"
          )
        }
      }
    catch {
      case e: WakeupException =>
        // Ignore exception if closing
        if (!closed.get) throw e
    }
    totalRecords
  }

  /** Main method to consume records from the broker.
    * @param consumer
    *   Kafka broker consumer
    * @param pollingTime
    *   Indicates how many seconds will the consumer be polling
    * @param topic
    *   Topic for the consumer to subscribe
    * @return
    *   Total consumed records
    */
  def consume(consumer: Consumer[String, String], pollingTime: Int, topic: String): Long = {
    // We use atomic boolean because we are going to share the value between threads.
    val closed = new AtomicBoolean(false)

    // We start consumer stopper thread.
    println(s"Polling during $pollingTime seconds ...")
    startConsumerStopper(consumer, closed, pollingTime)

    // We start polling
    val totalRecords = startPolling(consumer, closed, topic)
    totalRecords
  }

  override def beforeAll(): Unit = {
    if (!initErrorMsg.isBlank)
      fail(s"Properties were not loaded correctly - $initErrorMsg")

    assert(properties.size() > 0, "Properties were not loaded correctly")
  }

  test("There is a valid broker running") {
    println("Validating broker...")
    import org.scalatest.time.SpanSugar.convertIntToGrainOfTime
    failAfter(
      convertIntToGrainOfTime(
        properties.getOrDefault("testbroker.timeout", 10).toString.toInt
      ).seconds
    ) {
      // Every node represents a valid broker in kafka server.
      val nodeList = Admin.create(properties).describeCluster.nodes.get
      assert(!nodeList.isEmpty, "Brokers list is empty")
    }
  }

  test("Is the configured topic valid") {
    println("Validating topic...")
    val consumer = new KafkaConsumer[String, String](properties)
    val topicList: util.Map[String, util.List[PartitionInfo]] = consumer.listTopics
    consumer.close()

    /* If there is a valid broker running at least __consumer_offsets topic will be there.
     * (kafka version 5.8.2) */
    assert(
      topicList.containsKey(topic),
      s"There is no topic named $topic created on the server"
    )
  }

  test("Can subscribe and poll the configured topic") {
    println("Starting main broker consuming test...")

    val consumer = new KafkaConsumer[String, String](properties)

    /* We cannot add records before assigning a partition or subscribing to a topic.
     In this case we will subscribe to the configured topic. */
    val topicList: util.List[String] = Collections.singletonList(topic)
    consumer.subscribe(topicList)

    // Start polling
    val totalRecords =
      consume(
        consumer,
        properties.getOrDefault("polling.time", 10).toString.toInt,
        topic
      )

    if (totalRecords == 0) println(">>>> No records found during polling.")
    else println(">>>> Total records consumed: " + totalRecords)
    consumer.close()
  }

}
